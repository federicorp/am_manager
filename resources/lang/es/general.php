<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the system in general
    |
    */

    'general-error' => 'Ha ocurrido un error',
    'delete-dependent-data' => 'No se puede eliminar ya que existen datos que dependen de este dato.'
];
