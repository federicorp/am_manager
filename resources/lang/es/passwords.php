<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Tu contraseña fue reseteada!',
    'sent' => 'Hemos enviado el link de reseteo de contraseña a su correo.',
    'sent-mobile' => 'Hemos enviado un código de reseteo de contraseña a su número de teléfono.',
    'throttled' => 'Por favor espere unos segundos antes de volver a intentar.',
    'token' => 'Este token de reseteo de contraseña es invalido.',
    'user' => "No podemos encontrar el usuario con esa dirección de correo electrónico.",

];
