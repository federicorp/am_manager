<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no existen.',
    'user-password' => 'Usuario o contraseña incorrecta.',
    'password' => 'La contraseña es incorrecta.',
    'throttle' => 'Muchos intentos de ingreso. Por favor vuelva a intentarlo en :seconds segundos.',

];
