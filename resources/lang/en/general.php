<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the system in general
    |
    */

    'general-error' => 'An error has occurred.',
    'delete-dependent-data' => 'You can\'t delete this data since there are other depending on it'
];
