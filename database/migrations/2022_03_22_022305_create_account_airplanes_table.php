<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_airplanes', function (Blueprint $table) {
            $table->id();
            $table->string('am_id');
            $table->foreignId('airplane_id')->constrained();
            $table->foreignId('account_id')->constrained();
            $table->foreignId('account_circuit_id')->nullable()->constrained();
            $table->string('name');
            $table->json('configurations');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_airplanes');
    }
};
