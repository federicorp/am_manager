<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_circuits', function (Blueprint $table) {
            $table->enum('status', ['future', 'building', 'deploying', 'running', 'pending_revaluation', 'pending_redeployment', 'redeploying'])->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_circuits', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
};
