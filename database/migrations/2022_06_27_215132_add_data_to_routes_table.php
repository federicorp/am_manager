<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->foreignId('airport_from_id')->after('id');
            $table->foreignId('airport_to_id')->after('airport_from_id');
            $table->double('distance')->after('airport_to_id');

            $table->foreign('airport_from_id')->references('id')->on('airports');
            $table->foreign('airport_to_id')->references('id')->on('airports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routes', function (Blueprint $table) {

            $table->dropForeign('airport_from_id');
            $table->dropForeign('airport_to_id');

            $table->dropColumn('airport_from_id');
            $table->dropColumn('airport_to_id');
            $table->dropColumn('distance');
        });
    }
};
