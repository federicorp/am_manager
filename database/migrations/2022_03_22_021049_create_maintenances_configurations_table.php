<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenance_configurations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id')->constrained();
            $table->json('conditions');
            $table->boolean('active')->default(true);
            $table->integer('interval_check');
            $table->dateTime('last_checked')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenances_configurations');
    }
};
