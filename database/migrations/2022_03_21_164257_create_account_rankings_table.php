<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_rankings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id')->constrained();
            $table->mediumInteger('ranking_number')->nullable();
            $table->bigInteger('structural_profit_on_ranking')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_rankings');
    }
};
