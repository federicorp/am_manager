<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_hub_routes', function (Blueprint $table) {
            $table->json('current_demand')->nullable()->after('current_prices');
            $table->boolean('is_frozen')->default(false)->after('current_demand');
            $table->dateTime('locked_until')->nullable()->after('is_frozen');
            $table->dateTime('purchase_date')->nullable()->after('locked_until');
            $table->integer('purchase_price')->nullable()->after('purchase_date');
            $table->integer('selling_price')->nullable()->after('purchase_price');
            $table->json('remaining_demand')->nullable()->after('current_demand');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_hub_routes', function (Blueprint $table) {
            $table->dropColumn('current_demand');
            $table->dropColumn('is_frozen');
            $table->dropColumn('locked_until');
            $table->dropColumn('purchase_date');
            $table->dropColumn('purchase_price');
            $table->dropColumn('selling_price');
            $table->dropColumn('remaining_demand');
        });
    }
};
