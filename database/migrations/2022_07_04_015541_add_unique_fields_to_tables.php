<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('continents', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('countries', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('airports', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('account_hubs', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('account_hub_routes', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('account_airplanes', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('account_tasks', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('accounts', function (Blueprint $table) {
            $table->unique('am_account_id');
        });
        Schema::table('airplane_makers', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('airplanes', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('alliances', function (Blueprint $table) {
            $table->unique('am_alliance_id');
        });
        Schema::table('research', function (Blueprint $table) {
            $table->unique('am_id');
        });
        Schema::table('services', function (Blueprint $table) {
            $table->unique('am_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('continents', function (Blueprint $table) {
            $table->dropUnique('continents_am_id_unique');
        });
        Schema::table('countries', function (Blueprint $table) {
            $table->dropUnique('countries_am_id_unique');
        });
        Schema::table('airports', function (Blueprint $table) {
            $table->dropUnique('airports_am_id_unique');
        });
        Schema::table('account_hubs', function (Blueprint $table) {
            $table->dropUnique('account_hubs_am_id_unique');
        });
        Schema::table('account_hub_routes', function (Blueprint $table) {
            $table->dropUnique('account_hub_routes_am_id_unique');
        });
        Schema::table('account_airplanes', function (Blueprint $table) {
            $table->dropUnique('account_airplanes_am_id_unique');
        });
        Schema::table('account_tasks', function (Blueprint $table) {
            $table->dropUnique('account_tasks_am_id_unique');
        });
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropUnique('accounts_am_account_id_unique');
        });
        Schema::table('airplane_makers', function (Blueprint $table) {
            $table->dropUnique('airplane_makers_am_id_unique');
        });
        Schema::table('airplanes', function (Blueprint $table) {
            $table->dropUnique('airplanes_am_id_unique');
        });
        Schema::table('alliances', function (Blueprint $table) {
            $table->dropUnique('alliances_am_alliance_id_unique');
        });
        Schema::table('research', function (Blueprint $table) {
            $table->dropUnique('research_am_id_unique');
        });
        Schema::table('services', function (Blueprint $table) {
            $table->dropUnique('services_am_id_unique');
        });
    }
};
