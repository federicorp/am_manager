<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('airplanes', function (Blueprint $table) {
            $table->double('tonnage')->after('configurations');
            $table->integer('range')->after('tonnage');
            $table->double('speed')->after('range');
            $table->json('available_in')->after('speed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('airplanes', function (Blueprint $table) {
            $table->dropColumn('tonnage');
            $table->dropColumn('range');
            $table->dropColumn('speed');
            $table->dropColumn('available_in');
        });
    }
};
