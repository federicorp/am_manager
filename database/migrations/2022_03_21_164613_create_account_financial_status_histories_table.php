<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_financial_status_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id')->constrained();
            $table->bigInteger('turnover')->nullable();
            $table->bigInteger('flight_costs')->nullable();
            $table->bigInteger('results')->nullable();
            $table->bigInteger('loan_payment')->nullable();
            $table->bigInteger('lease_costs')->nullable();
            $table->bigInteger('structural_profit')->nullable();
            $table->bigInteger('rnd_investment')->nullable();
            $table->bigInteger('staff_cost')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_financial_status_histories');
    }
};
