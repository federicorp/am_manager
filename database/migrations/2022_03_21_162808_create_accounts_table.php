<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('am_account_id');
            $table->foreignId('user_id')->constrained();
            $table->foreignId('alliance_id')->nullable()->constrained();
            $table->string('name');
            $table->string('server');
            $table->date('born_at')->nullable();
            $table->bigInteger('value')->nullable();
            $table->bigInteger('balance')->nullable();
            $table->json('airline_configuration')->nullable();
            $table->bigInteger('structure_profit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
};
