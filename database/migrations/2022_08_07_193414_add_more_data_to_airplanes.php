<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('airplanes', function (Blueprint $table) {
            $table->dropColumn('configurations');
            $table->tinyInteger('category')->after('airplane_maker_id');
            $table->float('consumption')->after('category');
            $table->foreignId('haul_id')->after('consumption')->constrained();
            $table->string('image')->nullable()->after('haul_id');
            $table->boolean('classic')->default(false)->after('image');
            $table->boolean('full_cargo')->default(false)->after('classic');
            $table->float('wear_speed')->after('full_cargo');
            $table->float('payload')->after('wear_speed');
            $table->double('price')->after('payload');
            $table->json('seats')->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('airplanes', function (Blueprint $table) {

            $table->dropForeign('airplanes_haul_id_foreign');

            $table->json('configurations')->after('speed');
            $table->dropColumn('category');
            $table->dropColumn('consumption');
            $table->dropColumn('haul_id');
            $table->dropColumn('image');
            $table->dropColumn('classic');
            $table->dropColumn('full_cargo');
            $table->dropColumn('wear_speed');
            $table->dropColumn('payload');
            $table->dropColumn('price');
            $table->dropColumn('seats');
        });
    }
};
