<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_hubs', function(Blueprint $table){
            $table->dropForeign('account_hubs_hub_id_foreign');
        });
        Schema::rename('hubs', 'airports');

        Schema::table('account_hubs', function(Blueprint $table){
            $table->renameColumn('hub_id', 'airport_id');
            $table->foreign('airport_id')->references('id')->on('airports');
        });

        Schema::table('airports', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('airports', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('account_hubs', function(Blueprint $table){
            $table->dropForeign('account_hubs_airport_id_foreign');
        });
        Schema::rename('airports', 'hubs');

        Schema::table('account_hubs', function(Blueprint $table){
            $table->renameColumn('airport_id', 'hub_id');
            $table->foreign('hub_id')->references('id')->on('hubs');
        });
    }
};
