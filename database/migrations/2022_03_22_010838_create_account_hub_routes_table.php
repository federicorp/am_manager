<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_hub_routes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_hub_id')->constrained();
            $table->foreignId('route_id')->constrained();
            $table->foreignId('account_circuit_id')->nullable()->constrained();
            $table->json('last_audit')->nullable();
            $table->json('current_prices')->nullable();
            $table->tinyInteger('category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_hub_routes');
    }
};
