<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_hubs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id')->constrained();
            $table->foreignId('hub_id')->constrained();
            $table->string('am_id');
            $table->dateTime('purchased_at');
            $table->bigInteger('turn_over');
            $table->bigInteger('profits');
            $table->integer('incidents')->nullable();
            $table->bigInteger('incidents_costs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_hubs');
    }
};
