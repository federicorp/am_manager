# Add Configuration

POST {{host}}/v1/accounts/2/maintenance-configurations
Authorization: Bearer {{auth_key}}
Content-Type: multipart/form-data; boundary=WebAppBoundary
Accept: application/json

--WebAppBoundary
Content-Disposition: form-data; name="conditions"

< ./data/configuration.json
--WebAppBoundary
Content-Disposition: form-data; name="interval_check"

120
--WebAppBoundary--

> {%

    client.test("Request executed successfully", function() {
        client.assert(response.status === 200, response.body.message);
    });

%}

###
