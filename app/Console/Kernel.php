<?php

namespace App\Console;

use App\Jobs\Airplanes\GetAirplanesModel;
use App\Jobs\Airplanes\RefreshAccountsAirplanes;
use App\Jobs\Circuits\RefreshAirplaneCounts;
use App\Jobs\Hubs\RefreshAirportsInformation;
use App\Jobs\Hubs\RefreshHubsInfo;
use App\Jobs\Maintenances\CheckMaintenance;
use App\Jobs\Tasks\CheckTasks;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        /*-------------- CADA 1 MINUTO ----------*/

         $schedule->job(new CheckMaintenance())->everyMinute();

        /*---------------------------------------*/


        /*-------------- CADA 1 HORA ----------*/

        $schedule->job(new CheckTasks())->hourly();

        /*---------------------------------------*/


        /*-------------- CADA 1 DIA ----------*/

        $schedule->job(new GetAirplanesModel())->dailyAt('00:00'); // Refrescar info de airplanes
        $schedule->job(new RefreshAirportsInformation())->dailyAt('00:00'); // Refrescar info de airports
        $schedule->job(new RefreshHubsInfo())->dailyAt('01:00'); // Refrescar info de hubs de las cuentas
        $schedule->job(new RefreshAccountsAirplanes())->dailyAt('01:15'); // Refrescar airplanes de las cuentas
        $schedule->job(new RefreshAirplaneCounts())->dailyAt('02:00'); // Refrescar cantidad de AC en Circuitos

        /*---------------------------------------*/
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
