<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\MaintenanceConfiguration;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MaintenancesController extends Controller
{

    public function indexConfigurations(Account $account): JsonResponse
    {
        return response()->success(data: $account->maintenance_configurations()->paginate(), paginate: true);
    }

    public function createConfiguration(Request $request, Account $account): JsonResponse
    {

        $request->validate([
            'conditions' => 'required|json',
            'interval_check' => 'required|integer'
        ]);

        $validator = \Validator::make(json_decode($request->get('conditions', []), true), [
            'type' => 'required|string',
            'percentage' => 'required|numeric',
            'percentage_gt' => 'required|bool',
            'age' => 'required|numeric',
            'age_gt' => 'required|bool',
        ]);

        if ($validator->passes()) {
            $maintenanceConfiguration = $account->maintenance_configurations()
                ->create(array_merge(
                        $request->only('interval_check'),
                    [
                        'conditions' => json_decode($request->get('conditions', []), true)
                    ]
                ));
            return response()->success(data: $maintenanceConfiguration);
        }

        return response()->error(422, "Conditions malformed JSON or not complying.");
    }

    public function updateConfiguration(Request $request, Account $account, MaintenanceConfiguration $maintenanceConfiguration): JsonResponse
    {
        $request->validate([
            'conditions' => 'json',
            'active' => 'bool',
            'interval_check' => 'integer'
        ]);

        $validator = \Validator::make(json_decode($request->get('conditions', []), true), [
            'type' => 'required|string',
            'percentage' => 'required|numeric',
            'percentage_gt' => 'required|bool',
            'age' => 'required|numeric',
            'age_gt' => 'required|bool',
        ]);

        if ($validator->passes()) {

            $maintenanceConfiguration->update($request->only(['conditions', 'active', 'interval_check']));

            return response()->success(data: $maintenanceConfiguration);
        }

        return response()->error(422, "Conditions malformed JSON or not complying.");
    }

    public function deleteConfiguration(Request $request, Account $account, MaintenanceConfiguration $maintenanceConfiguration): JsonResponse
    {

        $maintenanceConfiguration->delete();

        return response()->success(data: $maintenanceConfiguration);
    }
}
