<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ConfirmAccountController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Confirm Account Controller
    |--------------------------------------------------------------------------
    |
    | Este controlador es para la confirmacion de correos o telefonos en las
    |  cuentas de los usuarios
    |
    | @author Federicorp <federicorp.py@gmail.com>
    |
    */

    /**
     * Confirmar la cuenta con un token que se le envia por correo
     *
     * @param Request $request
     * @param string $token
     * @return JsonResponse
     */
    public function confirm(Request $request, string $token): JsonResponse
    {
        return response()->error(400, 'NOT YET IMPLEMENTED');
    }
}
