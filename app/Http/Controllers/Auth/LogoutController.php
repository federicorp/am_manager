<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Logout Controller
    |--------------------------------------------------------------------------
    |
    | Este controlador realiza la eliminacion del token activo del usuario
    |
    | @author Federicorp <federicorp.py@gmail.com>
    |
    */

    public function logout(Request $request)
    {

        // Se da de baja el token del usuario
        $request->user()->currentAccessToken()->delete();

        return response()->success();
    }
}
