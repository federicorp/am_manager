<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PasswordRecoveryController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Password Recovery Controller
    |--------------------------------------------------------------------------
    |
    | Este controlador es para la recuperacion de passwords olvidados por los
    | usuarios
    |
    | @author Federicorp <federicorp.py@gmail.com>
    |
    */

    /**
     * Metodo para enviar el token de recuperación de password
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendRecover(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        return response()->error(400, 'NOT YET IMPLEMENTED');
    }
}
