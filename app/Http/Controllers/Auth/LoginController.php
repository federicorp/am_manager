<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | Este controlador realiza la autenticacion de los usuarios del sistema y
    | les da un access token para poder realizar request a rutas protegidas
    |
    | @author Federicorp <federicorp.py@gmail.com>
    |
    */

    public function login(Request $request)
    {

        $request->validate([
            'email' => 'required|string|max:200',
            'password' => 'required|min:6'
        ]);

        // Se intenta loguear al usuario
        if (Auth::attempt($request->only(['email', 'password']))) {

            // Se obtiene el token para el usuario
            $user = Auth::user();
            $token = $user->createToken('Acceso ' . Carbon::now()->format("Y-m-d H:i"));

            $user->access_token = $token->plainTextToken;

            // Se retornan los datos de acceso
            return response()->success(data: $user);
        }

        return response()->error(403, __('auth.user-password'));
    }
}
