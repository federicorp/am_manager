<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | Este controlador hace el registro de los nuevos usuarios
    |
    | @author Federicorp <federicorp.py@gmail.com>
    |
    */

    /**
     * Metodo para registrar usuarios
     *
     * @param Request $request
     * @param bool $returnOnlyData
     * @return JsonResponse|User
     */
    public function register(Request $request, bool $returnOnlyData = false): JsonResponse|User
    {
        $request->validate([
            'name' => 'required|string|max:200|min:3',
            'email' => 'required_without:phone_number|email|unique:users,email',
            'password' => 'required_without_all:account,account_name|confirmed|min:6',
        ]);

        // Se crea el usuario
        $user = new User(
            array_merge(
                $request->only(['name', 'email']),
                ['password' => !$request->exists('password') ?
                    User::PASSWORD_NOT_SET : Hash::make($request->get('password')),
                ]
            )
        );
        $user->save();

        // Se emite un evento de usuario registrado
        event(new Registered($user));

        if ($returnOnlyData)
            return $user;

        return response()->success(message: 'User created', data: $user);
    }
}
