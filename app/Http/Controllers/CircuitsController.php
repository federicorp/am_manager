<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountCircuit;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CircuitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Account $account
     * @return JsonResponse
     */
    public function index(Account $account): JsonResponse
    {
        if ($account->user_id === Auth::user()->id) {
            $accounts = AccountCircuit::whereAccountId($account->id);

            return response()->success(data: $accounts->paginate(15), paginate: true);
        } else {
            return response()->error(404, 'Account not found');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Account $account)
    {
        $request->validate([
            'name' => 'required|string',
            'hub' => 'required|exists:account_hubs,id,account_id,' . $account->id,
            'airplane_id' => 'required|exists:airplanes,id',
            'routes' => 'rule_required_without:type|array',
            'routes.*.id' => 'required_if:routes|exists:routes,id',
            'routes_amount' => 'required_without:routes',
            'type' => [Rule::in(['SH', 'MH', 'LH']), Rule::requiredWithout('routes')],
        ]);

        // Primera logica
        if (!$request->has('routes')) {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Account $account
     * @param AccountCircuit $circuit
     * @return JsonResponse
     */
    public function show(Account $account, AccountCircuit $circuit): JsonResponse
    {
        return response()->success(data: $circuit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
