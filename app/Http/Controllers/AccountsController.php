<?php

namespace App\Http\Controllers;

use App\Jobs\Airplanes\GetAccountAirplanes;
use App\Jobs\Authentication\RefreshAccountToken;
use App\Jobs\Hubs\GetAccountHubInformation;
use App\Models\Account;
use App\Models\Alliance;
use App\Retriever\RetrieverClient;
use Auth;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Log;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->success(data: Account::where('user_id', Auth::user()->id)->paginate(), paginate: true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'user' => 'required',
            'password' => 'required'
        ]);

        // Se intenta hacer login con el cliente de AM
        $retriever = new RetrieverClient();

        $response = $retriever->login($request->get('user'), $request->get('password'));

        // Se verifica si es que viene el accesstoken
        $jsonAuthResponse = json_decode($response, true);

        if (isset($jsonAuthResponse['access_token'])) {

            if (Account::whereAmAccountId($jsonAuthResponse['airlineId'])->exists())
                throw new Exception('Ya existe la cuenta');

            $retriever->setToken($jsonAuthResponse['access_token'])->setAirlineId($jsonAuthResponse['airlineId'])->setGameServer($jsonAuthResponse['gameServer']);

            // Se agarran mas datos
            $loadingResponse = $retriever->getFirstLoadingData();
            $jsonLoadingResponse = json_decode($loadingResponse, true);

            if (is_array($jsonLoadingResponse)) {

                DB::beginTransaction();

                try {

                    // Se crea la alianza si no existe todavia
                    $allianceJson = $jsonLoadingResponse['profile']['alliance'];
                    $alliance = Alliance::firstOrCreate([
                        'am_alliance_id' => $allianceJson['id']
                    ], [
                        'name' => $allianceJson['name']
                    ]);

                    $jsonAuthResponse['user'] = $request->get('user');
                    $jsonAuthResponse['pwd'] = $request->get('password');

                    // Se crea la cuenta con los datos recibidos
                    $account = Account::create([
                        'am_account_id' => $jsonAuthResponse['airlineId'],
                        'user_id' => Auth::user()->id,
                        'alliance_id' => $alliance->id,
                        'name' => $jsonLoadingResponse['profile']['name'],
                        'server' => $jsonAuthResponse['gameServer'],
                        'born_at' => $jsonLoadingResponse['profile']['registrationDate']['date'],
                        'structure_profit' => $jsonLoadingResponse['profile']['structuralProfit'],
                        'value' => $jsonLoadingResponse['profile']['valorization'],
                        'balance' => $jsonLoadingResponse['profile']['money'],
                        'airline_configuration' => [
                            'login' => $jsonAuthResponse,
                            'description' => $jsonLoadingResponse['profile']['description'],
                            'logo' => $jsonLoadingResponse['profile']['logoPath'],
                            'secretary' => [
                                'name' => $jsonLoadingResponse['profile']['secretary'],
                                'picture' => $jsonLoadingResponse['profile']['secretaryPicture'],
                            ],
                            'stars' => $jsonLoadingResponse['profile']['stars'],
                            'automatic_confirmation_events' => true,
                        ]
                    ]);

                    // Se setea una tarea para obtener la informacion de los hubs
                    dispatch((new GetAccountHubInformation($account))->delay(now()));

                    // Se setea una tarea para hacer el refresh del token
                    dispatch((new RefreshAccountToken($account))->delay(now()->addSeconds($jsonAuthResponse['expires_in'])->subMinutes(2)));

                    // Se setea una tarea para obtener la informacion de los airplanes
                    dispatch((new GetAccountAirplanes($account))->delay(now()));

                    DB::commit();

                    return response()->success(data: $account);
                } catch (Exception $e) {
                    Log::error($e);
                    DB::rollBack();
                }
            } else {
                return response()->error(statusCode: 400, message: 'Cuenta no agregada, error en la consulta.');
            }
        }

        return response()->error(statusCode: 400, message: 'Cuenta no agregada, datos inválidos.');
    }

    /**
     * Display the specified resource.
     *
     * @param Account $account
     * @return JsonResponse
     */
    public function show(Account $account): JsonResponse
    {
        $account->load('hubs');

        return response()->success(data: $account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return JsonResponse
     */
    public function update(): JsonResponse
    {
        return response()->error(400, 'NOT SUPPORTED');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Account $account
     * @return JsonResponse
     */
    public function destroy(Account $account): JsonResponse
    {

        $account->delete();

        return response()->success();
    }
}
