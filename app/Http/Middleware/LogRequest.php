<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class LogRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {

        $uuid = (string) Str::uuid();

        Log::info("Request $uuid: " . $request->getUri());

        $response = $next($request);

        // Loguea nuevamente
        Log::info("Responde request $uuid");
        Log::debug("Respuesta del request $uuid:", [$response]);

        return $response;
    }
}
