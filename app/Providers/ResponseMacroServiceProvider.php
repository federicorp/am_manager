<?php


namespace App\Providers;


use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

/**
 * Class ResponseMacroServiceProvider
 * @package App\Providers
 */
class ResponseMacroServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Estandarizar las respuestas JSON
        Response::macro("error", function (int $statusCode, string|null $message = '', $data = []) {

            if (request()->hasHeader('Accept') && request()->header('Accept') === 'application/json') {
                return Response::json([
                    'status' => 'error',
                    'message' => $message,
                    'data' => $data
                ], $statusCode);
            }
            abort($statusCode, $message);
            return null;
        });


        //Estandarizar las respuestas JSON
        Response::macro("success", function (int $statusCode = 200, string|null $message = '', $data = [], $paginate = false) {

            if (request()->hasHeader('Accept') && request()->header('Accept') === 'application/json') {

                $dataWord = $paginate ? 'pagination' : 'data';

                return Response::json([
                    'status' => 'ok',
                    'message' => $message,
                    $dataWord => $data
                ], $statusCode);
            }
            return Response::make($message)->setStatusCode($statusCode);
        });
    }
}
