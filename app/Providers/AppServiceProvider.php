<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        // Para migraciones
        Schema::defaultStringLength(190);


        // Logs de consultas realizadas a la BD
        DB::listen(function($query) {
            Log::debug($query->sql . " " . print_r($query->bindings, true));
        });

    }
}
