<?php

namespace App\Retriever;

use App\Jobs\Authentication\RefreshAccountToken;
use App\Jobs\Authentication\RestartAccountToken;
use App\Models\Account;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use Log;
use Psr\Http\Message\RequestInterface;

class RetrieverClient
{

    private Client $guzzleClient;

    private string $deviceId = '7849753cef850312aaee94c1c415a8ae';
    private string $clientSecret = '3bp2344u3b40k08kcwws4c0cw0gsssksoowckgccgggsgo0gsg';
    private string $clientId = '1_5mpo24vpbbswcgg0wc0cg0s8s00kk4sgkkso0css80wokkc4ks';

    private string $gameServer;

    private ?CookieJar $lastCookie = null;

    public function __construct(private string $airlineId = '', private string $token = '')
    {


        $stack = HandlerStack::create();
        // Log requests
        $stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            $contentsRequest = (string)$request->getBody();
            Log::debug('Request URL', [$request->getMethod(), $request->getUri()]);
            Log::debug('Request Contents', [$contentsRequest]);
            return $request;
        }));

        $this->guzzleClient = new Client([
            'verify' => false,
            'cookies' => true,
            'handler' => $stack,
            'headers' => [
                'User-Agent' => 'UnityPlayer/2020.3.15f2 (UnityWebRequest/1.0, libcurl/7.75.0-DEV)',
                'X-Unity-Version' => '2020.3.15f2',
            ]
        ]);


    }

    public static function withAirlineId($airlineId): RetrieverClient
    {
        $account = Account::whereAmAccountId($airlineId)->first();
        return self::withAccount($account);
    }

    public static function withAccount(Account $account)
    {
        $instance = new self($account->am_account_id, $account->airline_configuration['login']['access_token']);
        if ($account)
            return $instance->setGameServer($account->server);

        return $instance;
    }

    public function setToken($token): RetrieverClient
    {
        $this->token = $token;
        return $this;
    }

    public function setAirlineId($airlineId): RetrieverClient
    {
        $account = Account::whereAmAccountId($airlineId)->first();
        $this->airlineId = $airlineId;

        if ($account)
            return $this->setGameServer($account->server);
        return $this;
    }

    public function setGameServer(string $gameServer): RetrieverClient
    {
        $this->gameServer = $gameServer;
        return $this;
    }


    private function getResponse(Response $response): string
    {
        $body = $response?->getBody()->getContents();

        Log::info('Respuesta de AM Server', [$response?->getStatusCode(), $response?->getHeaders(), $body]);

        // se parsea la respuesta para ver si es error de token
        $responseJson = json_decode($body, true);
        if (is_array($responseJson) && isset($responseJson['status'], $responseJson['message']) && $responseJson['status'] === 0 && $responseJson['message'] === 'invalid_grant' && !str_contains($responseJson['fullErrorMessage'], 'Invalid username and password combination')) {

            // Se setea una tarea para hacer el refresh del token
            dispatch((new RefreshAccountToken(Account::whereAmAccountId($this->airlineId)->first()))->delay(now()));

        }

        return $body;
    }


    private function handleError(GuzzleException $e, $callback)
    {

        Log::error($e);

        if ($e->getCode() === 401) {

            // Se hace nuevamente el login
            $account = Account::whereAmAccountId($this->airlineId)->first();

            Log::debug('grfs', [$account->airline_configuration['login']]);

            if ($account != null && isset($account->airline_configuration['login']['user'], $account->airline_configuration['login']['pwd'])) {

                $response = $this->login($account->airline_configuration['login']['user'], $account->airline_configuration['login']['pwd']);
                $jsonAuthResponse = json_decode($response, true);

                // Se verifica si es que viene el accesstoken
                if (isset($jsonAuthResponse['access_token'])) {

                    $config = $account->airline_configuration;

                    $jsonAuthResponse['user'] = $account->airline_configuration['login']['user'];
                    $jsonAuthResponse['pwd'] = $account->airline_configuration['login']['pwd'];
                    $config['login'] = $jsonAuthResponse;
                    $account->airline_configuration = $config;
                    $account->save();

                    $this->token = $jsonAuthResponse['access_token'];

                    return $callback();
                }
            }
        }
        return null;
    }

    /**
     * Login
     *
     * @param string $username
     * @param string $password
     * @return null|string
     */
    public function login(string $username, string $password): ?string
    {

        $response = null;

        try {
            $response = $this->guzzleClient->request(
                'POST',
                'https://auth.airlines-manager.com/oauth/v2/token?version=30610',
                [
//                    'debug' => fopen(__DIR__ . '/debug.txt', 'w+'),
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                    'form_params' => [
                        'username' => $username,
                        'client_id' => $this->clientId,
                        'client_secret' => $this->clientSecret,
                        'password' => $password,
                        'grant_type' => 'password',
                        'device_id' => $this->deviceId,
                        'locale' => 'en',
                    ],
                    'cookies' => $this->lastCookie
                ]
            );
        } catch (GuzzleException $e) {
            Log::error($e);
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    /**
     * Refresh Token
     *
     * @return null|string
     */
    public function refreshToken(): ?string
    {

        $account = Account::whereAmAccountId($this->airlineId)->first();
        $refreshToken = $account->airline_configuration['login']['refresh_token'];

        $response = null;

        try {
            $response = $this->guzzleClient->request(
                'POST',
                'https://auth.airlines-manager.com/oauth/v2/token?version=30610',
                [
//                    'debug' => fopen(__DIR__ . '/debug.txt', 'w+'),
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                    'form_params' => [
                        'client_id' => $this->clientId,
                        'client_secret' => $this->clientSecret,
                        'refresh_token' => $refreshToken,
                        'grant_type' => 'refresh_token',
                        'device_id' => $this->deviceId,
                        'locale' => 'en',
                    ],
                    'cookies' => $this->lastCookie
                ]
            );

            $responseRefresh = $this->getResponse($response);
            $jsonAuthResponse = json_decode($responseRefresh, true);
            $config = $account->airline_configuration;
            $jsonAuthResponse['user'] = $account->airline_configuration['login']['user'];
            $jsonAuthResponse['pwd'] = $account->airline_configuration['login']['pwd'];
            $config['login'] = $jsonAuthResponse;
            $account->airline_configuration = $config;
            $account->save();

            // Se setea una tarea para hacer el refresh del token
            if (isset($jsonAuthResponse['expires_in'])) {
                dispatch((new RefreshAccountToken($account))->delay(now()->addSeconds($jsonAuthResponse['expires_in'])->subMinutes(2)));
            } else {
                dispatch((new RestartAccountToken($account))->delay(now()));
            }

        } catch (GuzzleException $e) {
            Log::error($e);
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }


    /**
     * @return string|null
     */
    public function getFirstLoadingData(): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/loading/1?deviceId=7849753cef850312aaee94c1c415a8ae&app_events[]=abtesting_move_cashflow&access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                    'cookies' => $this->lastCookie
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getFirstLoadingData());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }


    /**
     * @return string|null
     */
    public function getAirlineHubsData(): ?string
    {


        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/bfa/hub?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                    'cookies' => $this->lastCookie
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getAirlineHubsData());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }


    /**
     * @return string|null
     */
    public function getWorldHubsData(): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/bfa/world?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                    'cookies' => $this->lastCookie
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getWorldHubsData());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }


    /**
     * @param $wear
     * @param $age
     * @return string|null
     */
    public function getPreMaintenanceData($wear, $age): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . "/maintenance/aircrafts?wearMin=$wear&markMin=$age&access_token=" . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                    'cookies' => $this->lastCookie
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getPreMaintenanceData($wear, $age));
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    /**
     * @param String $type
     * @return string|null
     */
    public function doMaintenance(string $type): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'POST',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/maintenance/aircrafts?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                    'form_params' => [
                        'check' => $type,
                        'haul' => [
                            'long', 'medium', 'short',
                        ]
                    ],
                    'cookies' => $this->lastCookie
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->doMaintenance($type));
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getEvents(): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/event?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getEvents());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    /**
     * @param String $eventId
     * @return string|null
     */
    public function validateEvent(string $eventId): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'POST',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/event/validate?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                    'form_params' => [
                        'eventId[]' => $eventId
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->validateEvent($eventId));
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getAirplanesModel(): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/bfa/model?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getAirplanesModel());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    public function getAirplanesBuyList(): ?string
    {
        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/aircraft/buy/list/all_haul?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getAirplanesModel());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getAccountAirplanes(): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/bfa/aircraft?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getAccountAirplanes());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    /**
     * @param $aircraftId
     * @return string|null
     */
    public function getAirplanePlanning($aircraftId): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/planning/lines/' . $aircraftId . '?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getAccountAirplanes());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }

    /**
     * @param $aircraftId
     * @return string|null
     */
    public function getRouteInfo($routeId): ?string
    {

        try {
            $response = $this->guzzleClient->request(
                'GET',
                'https://' . $this->gameServer . '/api/' . $this->airlineId . '/line/' . $routeId . '?access_token=' . $this->token,
                [
                    'header' => [
                        'Accept' => '*/*',
                        'Accept-Encoding' => 'deflate, gzip'
                    ],
                ]
            );
        } catch (GuzzleException $e) {
            $response = $this->handleError($e, fn() => $this->getAccountAirplanes());
        }

        if ($response?->getStatusCode() >= 200 && $response?->getStatusCode() <= 299) {
            return $this->getResponse($response);
        }

        return null;
    }
}
