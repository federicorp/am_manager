<?php

namespace App\Jobs\Hubs;

use App\Models\Account;
use App\Models\AccountHub;
use App\Models\Airport;
use App\Retriever\RetrieverClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetAccountHubInformation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private Account $account)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Verificar si hay airports en el sistema, sino, estirar primero los airports
        if (Airport::count() == 0) {
            dispatch((new RefreshAirportsInformation())->delay(now()));

            // Luego de estirar la info, hacer nuevamente este proceso de hubs
            dispatch((new GetAccountHubInformation($this->account))->delay(now()->addMinutes(10)));

        } else {
            $retriever = RetrieverClient::withAccount($this->account);

            // Se estiran los hubs de la cuenta
            $hubsDataResponse = $retriever->getAirlineHubsData();
            $jsonHubsResponse = json_decode($hubsDataResponse, true);

            if (is_array($jsonHubsResponse)) {
                foreach ($jsonHubsResponse['hubList'] as $hubJson) {

                    $airport = Airport::where('am_id', $hubJson['a1'])->first();

                    AccountHub::firstOrCreate([
                        'am_id' => $hubJson['id'],
                    ],[
                        'account_id' => $this->account->id,
                        'airport_id' => $airport->id,
                        'purchased_at' => $hubJson['pDate']['date'],
                        'turn_over' => $hubJson['ca'],
                        'profits' => $hubJson['ben'],
                    ]);
                }
            }

            // Se estira la info de las rutas de cada uno de los hubs
            dispatch((new RefreshAccountRoutes($this->account))->delay(now()->addSeconds(5)));
        }
    }
}
