<?php

namespace App\Jobs\Hubs;

use App\Models\AccountHubRoute;
use App\Retriever\RetrieverClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class RefreshRouteExtendedInfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private string $routeAmId)
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Se obtienen datos de la ruta
        $route = AccountHubRoute::with('accountHub.account')->whereAmId($this->routeAmId)->first();

        if ($route) {

            // Para evitar spammear al server, solo se obtienen cada 10 dias la info
            if ($route->last_audit != null && $route->updated_at->addDays(10) > now()) {
                Log::warning('Todavia no paso 10 dias desde la ultima vez que se actualizo la ruta');
            }

            // Se obtiene la instancia del retriever
            $retriever = RetrieverClient::withAccount($route->accountHub->account);

            // Se obtienen los datos del server
            $routeResponse = $retriever->getRouteInfo($this->routeAmId);
            $jsonRouteResponse = json_decode($routeResponse, true);

            if (is_array($jsonRouteResponse)) {

                $line = $jsonRouteResponse['line'];

                $route->update([
                    'last_audit' => $line['audit'],
                    'current_prices' => $line['price'],
                    'current_demand' => $line['demand'],
                    'remaining_demand' => $line['remainingDemand'],
                    'is_frozen' => $line['isFrozen'],
                    'locked_until' => $line['lockedUntil']['date'],
                    'purchase_date' => $line['purchaseDate']['date'],
                    'purchase_price' => $line['purchasePrice'],
                    'selling_price' => $line['sellingPrice'],
                ]);
            }
        }
    }
}
