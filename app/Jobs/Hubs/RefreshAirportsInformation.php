<?php

namespace App\Jobs\Hubs;

use App\Models\Account;
use App\Models\Airport;
use App\Models\Continent;
use App\Models\Country;
use App\Retriever\RetrieverClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class RefreshAirportsInformation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private array $cacheWorld = [];
    private array $cacheContinents = [];
    private array $cacheCountries = [];

    private RetrieverClient $retrieverClient;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtener un airlineId Aleatorio
        $airline = Account::inRandomOrder()->first();

        // Se obtienen los datos del mundo en el server
        if (count($this->cacheWorld) > 0) $responseJson = $this->cacheWorld;
        else {
            $responseWorld = ($this->retrieverClient = RetrieverClient::withAirlineId($airline->am_account_id))->getWorldHubsData();

            $responseJson = json_decode($responseWorld, true);
            if ($responseJson !== null) {
                $this->cacheWorld = $responseJson;
            }
        }

        if ($responseJson !== null) {

            $arrayToInsert = [];

            $airportExistent = array_map(fn($el) => $el->am_id, Airport::whereIn('am_id', array_map(fn($el) => $el['id'], $responseJson['airportList']))
                ->get('id')->all());

            foreach ($responseJson['airportList'] as $airport) {

                $country = $this->getCountry($airport['ctry'] ?? $airport['ty']);

                Log::debug('country for ' . ($airport['ctry'] ?? $airport['ty']), [$country]);

                if (!in_array($airport['id'], $airportExistent)) {
                    $arrayToInsert[] = [
                        'am_id' => $airport['id'],
                        'name' => $airport['n'],
                        'iata_code' => $airport['i'],
                        'category' => $airport['cat'],
                        'latitude' => $airport['lt'],
                        'longitude' => $airport['lg'],
                        'country_id' => $country?->id,
                    ];
                }
            }

            Airport::insertOrIgnore($arrayToInsert);

            $airports = Airport::get()->all();

            // Se crean las rutas

            $c = 0;
            foreach ($airports as $airport) {
                dispatch((new UpdateRoutes($airport))->delay(now()->addSeconds(($c++ / 10))));
            }
        }
    }


    private function getCountry($amId): Country|null
    {

        if (isset($this->cacheCountries[$amId])) return $this->cacheCountries[$amId];

        $this->cacheCountries[$amId] = Country::where('am_id', $amId)->first();
        if ($this->cacheCountries[$amId] !== null) return $this->cacheCountries[$amId];

        // Se obtienen los datos del mundo en el server
        $responseJson = $this->getAllWorldData();


        if ($responseJson !== null) {

            // Se guardan todos los countries

            $arrayToInsert = [];

            $countryExistent = array_map(fn($el) => $el->am_id, Country::whereIn('am_id', array_map(fn($el) => $el['id'], $responseJson['countryList']))
                ->get('id')->all());

            foreach ($responseJson['countryList'] as $countryRaw) {

                $countryChArray = [$countryRaw];

                if (isset($countryRaw['ch'])) {
                    $countryChArray = array_merge($countryChArray, $countryRaw['ch']);
                }

                foreach ($countryChArray as $country) {

                    if (!in_array($country['id'], $countryExistent)) {
                        $arrayToInsert[] = [
                            'am_id' => $country['id'],
                            'continent_id' => $this->getContinent($country['cont'])->id,
                            'code' => $country['c2'] ?? $country['c'],
                            'name' => $country['n'],
                            'old_am_id' => $country['old'][0] ?? ($country['old'] ?? null),
                        ];
                    }
                }
            }

            Country::insertOrIgnore($arrayToInsert);
        }

        $this->cacheCountries[$amId] = Country::where('am_id', $amId)->first();

        return $this->cacheCountries[$amId];
    }

    private function getContinent($amId): Continent|null
    {

        if (isset($this->cacheContinents[$amId])) return $this->cacheContinents[$amId];

        $continent = Continent::where('am_id', $amId)->first();
        if ($continent !== null) {
            $this->cacheContinents[$amId] = $continent;
            return $continent;
        }


        // Se obtienen los datos del mundo en el server
        $responseJson = $this->getAllWorldData();


        if ($responseJson !== null) {

            $continentExistent = array_map(fn($el) => $el->am_id, Continent::whereIn('am_id', array_map(fn($el) => $el['id'], $responseJson['continentList']))
                ->get('id')->all());

            $arrayToInsert = [];

            // Se recorren los continentes
            foreach ($responseJson['continentList'] as $continent) {
                if (!in_array($continent['id'], $continentExistent)) {
                    $arrayToInsert[] = [
                        'am_id' => $continent['id'],
                        'name' => $continent['n'],
                        'code' => $continent['s'],
                    ];
                }
            }

            Continent::insertOrIgnore($arrayToInsert);
        }

        $this->cacheContinents[$amId] = Continent::where('am_id', $amId)->first();

        return $this->cacheContinents[$amId];
    }

    private function getAllWorldData()
    {
        // Se obtienen los datos del mundo en el server
        if (count($this->cacheWorld) > 0) $responseJson = $this->cacheWorld;
        else {
            $responseWorld = $this->retrieverClient->getWorldHubsData();

            $responseJson = json_decode($responseWorld, true);
            if ($responseJson !== null) {
                $this->cacheWorld = $responseJson;
            }
        }

        return $responseJson;
    }
}
