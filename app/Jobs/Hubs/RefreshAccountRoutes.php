<?php

namespace App\Jobs\Hubs;

use App\Models\Account;
use App\Models\AccountHub;
use App\Models\AccountHubRoute;
use App\Models\Airport;
use App\Models\Route;
use App\Retriever\RetrieverClient;
use Cache;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RefreshAccountRoutes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private Account $account)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $retriever = RetrieverClient::withAccount($this->account);

        $loadingResponse = $retriever->getFirstLoadingData();
        $jsonLoadingResponse = json_decode($loadingResponse, true);

        if (is_array($jsonLoadingResponse)) {

            if (is_array($jsonLoadingResponse['profile']['lines'])) {

                $arrayToInsert = [];

                $i = 0;
                foreach ($jsonLoadingResponse['profile']['lines'] as $line) {

                    $airportFrom = Cache::remember('airport' . $line['airports']['start']['id'], 60, fn() => Airport::whereAmId($line['airports']['start']['id'])->first());
                    $airportTo = Cache::remember('airport' . $line['airports']['end']['id'], 60, fn() => Airport::whereAmId($line['airports']['end']['id'])->first());
                    $route = Route::whereAirportFromId($airportFrom->id)->whereAirportToId($airportTo->id)->first();
                    $hub = Cache::remember('accountHub' . $airportFrom->id . '_' . $this->account->am_account_id, 60, fn() => AccountHub::whereAccountId($this->account->id)->whereAirportId($airportFrom->id)->first());

                    $arrayToInsert[] = [
                        'am_id' => $line['id'],
                        'account_hub_id' => $hub->id,
                        'route_id' => $route->id,
                        'account_circuit_id' => null,
                    ];

                    dispatch((new RefreshRouteExtendedInfo($line['id']))->delay(now()->addMinutes(5)->addSeconds($i += 3)));
                }

                AccountHubRoute::insertOrIgnore($arrayToInsert);

            }
        }
    }
}
