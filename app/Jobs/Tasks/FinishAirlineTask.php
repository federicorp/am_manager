<?php

namespace App\Jobs\Tasks;

use App\Models\Account;
use App\Retriever\RetrieverClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FinishAirlineTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private string $airlineId, private string $amTaskId)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $account = Account::whereAmAccountId($this->airlineId)->first();

        // Se validan los datos del Evento
        $eventsResponse = RetrieverClient::withAirlineId($this->airlineId)->validateEvent($this->amTaskId);
        $responseJson = json_decode($eventsResponse, true);

        if ($responseJson !== null && $responseJson['status'] === 1 && in_array($this->amTaskId, $responseJson['eventIds'])) {

            // Se elimina el task del account
            $account->tasks()->whereAmId($this->amTaskId)->delete();

        } else {

            \Log::error('Error en la respuesta del finish', [$eventsResponse]);

            throw new \Exception('No se encuentra el eventId en la respuesta');
        }

    }
}
