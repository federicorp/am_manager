<?php

namespace App\Jobs\Tasks;

use App\Models\Account;
use App\Models\AccountTask;
use App\Retriever\RetrieverClient;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckAirlineTasks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private string $airlineId, private string $token)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $account = Account::whereAmAccountId($this->airlineId)->first();

        // Se obtienen del servidor los datos
        $eventsResponse = RetrieverClient::withAirlineId($this->airlineId)->getEvents();
        $responseJson = json_decode($eventsResponse, true);

        if ($responseJson !== null && isset($responseJson['status']) && $responseJson['status'] == 1) {

            $list = $responseJson['list'];

            if (is_array($list) && count($list) > 0) {

                foreach ($list as $item) {

                    // Verifica si ya hay un task guardado ya para el evento
                    $task = $account->tasks()->whereAmId($item['id'])->first();

                    if (!$task) {

                        $date = Carbon::createFromFormat('Y-m-d H:i:s.u', $item['finishAt']['date'], 'UTC');

                        $account->tasks()->create([
                            'am_id' => $item['id'],
                            'am_label' => $item['label'],
                            'finish_date' => $date
                        ]);

                        dispatch((new FinishAirlineTask($this->airlineId, $item['id']))->delay($date));
                    }
                }
            } else {
                $account->tasks()->delete();
            }
        }
    }
}
