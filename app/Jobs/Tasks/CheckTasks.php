<?php

namespace App\Jobs\Tasks;

use App\Jobs\Authentication\RestartAccountToken;
use App\Models\Account;
use App\Retriever\RetrieverClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckTasks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Se obtienen todas las cuentas de aerolineas y se iteran entre ellas.
        $accounts = Account::where('airline_configuration->automatic_confirmation_events', true)->get()->all();


        foreach ($accounts as $account) {

            /**
             * @var Account $account
             */

            if (!isset($account->airline_configuration['login']['access_token'])) {
                dispatch((new RestartAccountToken($account))->delay(now()));
            } else {

                dispatch((new CheckAirlineTasks($account->am_account_id, $account->airline_configuration['login']['access_token']))->delay(now()));
            }
        }

    }
}
