<?php

namespace App\Jobs\Authentication;

use App\Models\Account;
use App\Retriever\RetrieverClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RestartAccountToken implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private Account $account)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = (new RetrieverClient())->login($this->account->airline_configuration['login']['user'], $this->account->airline_configuration['login']['pwd']);

        // Se verifica si es que viene el accesstoken
        $jsonAuthResponse = json_decode($response, true);

        if (isset($jsonAuthResponse['access_token'])) {

            $jsonAuthResponse['user'] = $this->account->airline_configuration['login']['user'];
            $jsonAuthResponse['pwd'] = $this->account->airline_configuration['login']['pwd'];

            $jsonToSave = $this->account->airline_configuration;
            $jsonToSave['login'] = $jsonAuthResponse;

            $this->account->airline_configuration = $jsonToSave;
            $this->account->save();

            dispatch((new RefreshAccountToken($this->account))->delay(now()->addSeconds($jsonAuthResponse['expires_in'])->subMinutes(2)));
        }


    }
}
