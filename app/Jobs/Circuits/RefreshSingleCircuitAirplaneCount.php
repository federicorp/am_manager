<?php

namespace App\Jobs\Circuits;

use App\Models\AccountAirplane;
use App\Models\AccountCircuit;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RefreshSingleCircuitAirplaneCount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private AccountCircuit $accountCircuit)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $configuration = $this->accountCircuit->configuration;

        $configuration['airplanes']['amount'] = AccountAirplane::whereAccountCircuitId($this->accountCircuit->id)->count();

        $this->accountCircuit->update([
            'configuration' => $configuration
        ]);
    }
}
