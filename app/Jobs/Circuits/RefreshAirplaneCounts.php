<?php

namespace App\Jobs\Circuits;

use App\Models\AccountCircuit;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RefreshAirplaneCounts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Por cada circuito
        $accountCircuits = AccountCircuit::get()->all();

        foreach ($accountCircuits as $circuit) {
            dispatch((new RefreshSingleCircuitAirplaneCount($circuit))->delay(now()));
        }
    }
}
