<?php

namespace App\Jobs\Circuits;

use App\Jobs\Hubs\RefreshRouteExtendedInfo;
use App\Models\Account;
use App\Models\AccountAirplane;
use App\Models\AccountCircuit;
use App\Models\AccountHub;
use App\Models\AccountHubRoute;
use App\Models\Airport;
use App\Models\Route;
use App\Retriever\RetrieverClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class GetCircuitInfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private RetrieverClient $retriever;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private Account $account, private array $airplaneJson)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->retriever = RetrieverClient::withAccount($this->account);

        $circuitId = $this->getCircuitId($this->airplaneJson);

        AccountAirplane::whereAmId($this->airplaneJson['id'])->update([
            'account_circuit_id' => $circuitId
        ]);
    }

    private function getCircuitId($airplane)
    {

        // Obtener datos del hub
        $hub = $this->getHub($airplane['h_id']);

        $airplanePlanningResponse = $this->retriever->getAirplanePlanning($airplane['id']);

        $jsonAirplanesPlanning = json_decode($airplanePlanningResponse, true);

        if (is_array($jsonAirplanesPlanning)) {
            $plannings = $jsonAirplanesPlanning['aircraft']['plannings'];

            if (count($plannings) > 0) {
                // Buscar si es que hay un circuito con la primera ruta del avion
                $accountHubRoute = AccountHubRoute::whereAmId($plannings[0]['lineId'])->first();

                if ($accountHubRoute != null && $accountHubRoute->account_circuit_id != null) {
                    return $accountHubRoute->account_circuit_id;
                } else {

                    $firstTime = true;

                    $accountCircuit = null;
                    foreach ($plannings as $planning) {

                        $route = Route::whereAirportFromId(Airport::whereIataCode($planning['iata1'])->first()->id)
                            ->whereAirportToId(Airport::whereIataCode($planning['iata2'])->first()->id)->first();

                        $routeDistance = $route->distance;

                        if ($firstTime) {
                            // Se crea el circuito

                            $accountCircuit = AccountCircuit::create([
                                'account_id' => $this->account->id,
                                'name' => 'Circuit ' . (AccountCircuit::whereAccountId($this->account->id)->count() + 1),
                                'type' => $routeDistance > (7540 * 1000) ? 'LH' :
                                    ($routeDistance <= (7540 * 1000) && $routeDistance > (3699 * 1000) ? 'MH' : 'SH'),
                                'status' => 'running',
                                'configuration' => [
                                    'airplanes' => [
                                        'amount' => 0,
                                        'economy' => $airplane['se'],
                                        'business' => $airplane['sb'],
                                        'first' => $airplane['sf'],
                                        'cargo' => $airplane['sp'],
                                    ],
                                ]
                            ]);

                            $firstTime = false;
                        }


                        if ($accountHubRoute == null) {
                            $accountHubRoute = AccountHubRoute::create([
                                'am_id' => $planning['lineId'],
                                'account_hub_id' => $hub->id,
                                'route_id' => $route->id,
                                'account_circuit_id' => $accountCircuit?->id,
                            ]);

                            dispatch((new RefreshRouteExtendedInfo($planning['lineId']))->delay(now()->addMinutes(5)));

                        } else {
                            $accountHubRoute = AccountHubRoute::whereAmId($planning['lineId'])->update([
                                'account_circuit_id' => $accountCircuit?->id,
                            ]);
                        }

                    }

                    return $accountCircuit?->id;
                }
            } else {
                Log::warning('Avion ' . $airplane['n'] . 'no tiene ninguna planificacion activa');
            }
        }

        return null;
    }

    private array $_cacheHub = [];

    /**
     * @param $amId
     * @return AccountHub
     */
    private function getHub($amId)
    {
        if (isset($_cacheHub[$amId])) return $_cacheHub[$amId];

        return $this->_cacheHub[$amId] = AccountHub::whereAmId($amId)->first();
    }
}
