<?php

namespace App\Jobs\Maintenances;

use App\Helpers\MaintenanceHelper;
use App\Jobs\Tasks\CheckTasks;
use App\Models\MaintenanceConfiguration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DoMaintenance implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private MaintenanceConfiguration $maintenanceConfiguration)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->maintenanceConfiguration->refresh();
        $this->maintenanceConfiguration->load('account');

        // Se comprueba nuevamente si es que ya se hizo el mantenimiento
        if ($this->maintenanceConfiguration->last_checked < now()->subMinutes($this->maintenanceConfiguration->interval_check)) {

            // Si aun no se hizo el mantenimiento, se hace ahora.
            $this->maintenanceConfiguration->last_checked = now();
            $this->maintenanceConfiguration->save();


            // Hacer el mantenimiento
            $maintenanceHelper = new MaintenanceHelper();
            if ($maintenanceHelper->doMaintenance($this->maintenanceConfiguration->conditions['percentage'], $this->maintenanceConfiguration->conditions['age'], $this->maintenanceConfiguration->conditions['type'], $this->maintenanceConfiguration->account->am_account_id)) {
                \Log::debug('Se hizo el mantenimiento para la configuracion', [$this->maintenanceConfiguration->conditions]);

                // Se comprueban eventos nuevos al terminar de hacer el mantenimiento
                dispatch((new CheckTasks())->delay(now()));
            } else {
                \Log::info('No se hizo el mantenimiento para la configuracion', [$this->maintenanceConfiguration]);
            }

        }

    }
}
