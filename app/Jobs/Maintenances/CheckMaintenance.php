<?php

namespace App\Jobs\Maintenances;

use App\Models\MaintenanceConfiguration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckMaintenance implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Se traen todos los mantenimientos que se tienen que hacer
        $maintenances = MaintenanceConfiguration::whereLastChecked(null)
            ->orWhere('last_checked', '<', \DB::raw('SUBDATE(now(), INTERVAL interval_check MINUTE)'))
            ->get()->all();

        foreach ($maintenances as $maintenance) {
            dispatch((new DoMaintenance($maintenance))->delay(now()));
        }
    }
}
