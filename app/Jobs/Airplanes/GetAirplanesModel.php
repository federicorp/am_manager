<?php

namespace App\Jobs\Airplanes;

use App\Models\Account;
use App\Models\Airplane;
use App\Models\AirplaneMaker;
use App\Retriever\RetrieverClient;
use Cache;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class GetAirplanesModel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Para AM normal y Tycoon
        $types = ['www', 'tycoon-api'];

        foreach ($types as $type) {

            // Se obtiene un authId aleatorio para obtener los aviones disponibles
            $account = Account::whereServer($type.'.airlines-manager.com')->inRandomOrder()->first();

            if ($account) {

                $retriever = RetrieverClient::withAccount($account);

                $obtainedModels = json_decode($retriever->getAirplanesBuyList(), true);

                var_dump($obtainedModels);

                foreach ($obtainedModels['aircrafts'] as $model) {

                    // Si aun no esta insertado en BD el model maker, se inserta
                    $airplaneMaker = Cache::remember('airplane_maker' . $model['manuf']['id'], 1000, function () use ($model) {

                        return AirplaneMaker::firstOrCreate([
                            'am_id' => $model['manuf']['id'],
                        ],
                            [
                                'name' => $model['manuf']['n']
                            ]);

                    });
                    if ($airplaneMaker) {

                        Airplane::updateOrCreate([
                            'am_id' => $model['id']
                        ], [
                            'name' => $model['n'],
                            'airplane_maker_id' => $airplaneMaker->id,
                            'configurations' => [],
                            'tonnage' => $model['p'],
                            'range' => $model['r'],
                            'speed' => $model['sp'],
                            'available_in' => ['www', 'tycoon'],
                        ]);

                    } else {
                        Log::warning('No se encuentra el airplane maker.', $model['manuf']);
                    }
                }
            }
        }
    }
}
