<?php

namespace App\Jobs\Airplanes;

use App\Models\Account;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RefreshAccountsAirplanes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtener todas las cuentas del sistema y por cada una de ellas, llamar al GetAccountHubInformation
        /**
         * @var Account $account
         */
        foreach (Account::all() as $account) {
            dispatch(new GetAccountAirplanes($account))->delay(now());
        }
    }
}
