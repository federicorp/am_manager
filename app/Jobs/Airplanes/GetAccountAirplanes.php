<?php

namespace App\Jobs\Airplanes;

use App\Jobs\Circuits\GetCircuitInfo;
use App\Models\Account;
use App\Models\AccountAirplane;
use App\Models\Airplane;
use App\Retriever\RetrieverClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetAccountAirplanes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private RetrieverClient $retriever;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private Account $account)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->retriever = RetrieverClient::withAccount($this->account);

        // Verificar si hay airplanes en el sistema, sino, estirar primero los airplanes
        if (Airplane::count() == 0) {
            dispatch((new GetAirplanesModel())->delay(now()));

            // Luego de estirar la info, hacer nuevamente este proceso de hubs
            dispatch((new GetAccountAirplanes($this->account))->delay(now()->addMinutes(10)));

        } else {

            // Se estiran los airplanes de la cuenta
            $airplanesDataResponse = $this->retriever->getAccountAirplanes();

            $jsonAirplanesResponse = json_decode($airplanesDataResponse, true);

            if (is_array($jsonAirplanesResponse)) {

                $i = 3;

                $existentData = [];
                foreach ($jsonAirplanesResponse['aircraftList'] as $airplaneJson) {

                    $airplane = AccountAirplane::updateOrCreate([
                        'am_id' => $airplaneJson['id']
                    ], [
                        'airplane_id' => $this->getAirplaneModel($airplaneJson['al_id'])->id,
                        'account_id' => $this->account->id,
                        'name' => $airplaneJson['n'],
                        'configurations' => [
                            'seats' => [
                                'economy' => $airplaneJson['se'],
                                'business' => $airplaneJson['sb'],
                                'first' => $airplaneJson['sf'],
                                'cargo' => $airplaneJson['sp'],
                            ],
                            'wear' => $airplaneJson['w'],
                            'hub_id' => $airplaneJson['h_id'],
                            'cumulative_result' => $airplaneJson['bs']
                        ]
                    ]);

                    if ($airplane->account_circuit_id == null) {
                        dispatch((new GetCircuitInfo($this->account, $airplaneJson))->delay(now()->addSeconds($i += 3)));
                    }

                    $existentData[] = $airplaneJson['id'];
                }

                // Se borran todos los airplanes que ya no existen
                AccountAirplane::whereNotIn('am_id', $existentData)->delete();
            }
        }
    }

    private array $_cacheAirplaneModel = [];

    private function getAirplaneModel($amId)
    {
        if (isset($_cacheAirplaneModel[$amId])) return $_cacheAirplaneModel[$amId];

        return $this->_cacheAirplaneModel[$amId] = Airplane::whereAmId($amId)->first();
    }
}
