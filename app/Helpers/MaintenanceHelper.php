<?php

namespace App\Helpers;

use App\Retriever\RetrieverClient;

class MaintenanceHelper
{

    public function __construct()
    {
    }

    public function doMaintenance($wear, $age, $type, $airlineId): bool
    {

        $retriever = RetrieverClient::withAirlineId($airlineId);

        $responsePreMaintenance = $retriever->getPreMaintenanceData($wear, $age);
        $responseJson = json_decode($responsePreMaintenance, true);

        if ($responseJson !== null) {

            if (isset($responseJson['maintenanceAircraftsToRepair'])) {

                $amount = array_reduce(array_map(function ($data) {

                    if (isset($data['maintenance']) && count($data['maintenance']) > 0) {
                        $longHaul = $data['maintenance']['longHaul'] ?? ['totalCount' => 0];
                        $mediumHaul = $data['maintenance']['mediumHaul'] ?? ['totalCount' => 0];
                        $shortHaul = $data['maintenance']['shortHaul'] ?? ['totalCount' => 0];
                        return $longHaul['totalCount'] + $mediumHaul['totalCount'] + $shortHaul['totalCount'];
                    }
                    return 0;
                }, $responseJson['maintenanceAircraftsToRepair']),
                function($carry, $item) {
                    $carry += $item;
                    return $carry;
                });


                if ($amount > 0) {

                    $responseMaintenance = $retriever->doMaintenance($type);
                    $responseMaintenanceJson = json_decode($responseMaintenance, true);

                    if ($responseMaintenanceJson !== null && $responseMaintenanceJson['status'] === 1 && $responseMaintenanceJson['message'] === 'aircraft.repair.success') {

                        return true;

                    } else {
                        \Log::warning('No se hizo el mantenimiento', [$responseMaintenance]);
                    }


                } else {
                    \Log::info('No hay aviones para mantenimiento');
                }
            }
        } else {
            \Log::error('Error de parseado');
        }

        return false;
    }

}
