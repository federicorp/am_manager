<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountHubRoute extends Model
{
    use HasFactory;

    protected $fillable = [
        'am_id',
        'account_hub_id',
        'route_id',
        'last_audit',
        'current_prices',
        'current_demand',
        'remaining_demand',
        'is_frozen',
        'locked_until',
        'purchase_date',
        'purchase_price',
        'selling_price',
        'account_circuit_id',
    ];

    protected $casts = [
        'last_audit' => 'json',
        'current_prices' => 'json',
        'current_demand' => 'json',
        'remaining_demand' => 'json',
        'updated_at' => 'date',
    ];



    public function accountHub()
    {
        return $this->belongsTo(AccountHub::class);
    }

    public function route()
    {
        return $this->belongsTo(Route::class);
    }
}
