<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Airplane extends Model
{
    use HasFactory;

    protected $fillable = ['am_id', 'name', 'airplane_maker_id', 'configurations', 'tonnage', 'range', 'speed', 'available_in'];

    protected $casts = [
        'configurations' => 'json',
        'available_in' => 'json',
    ];

    /**
     * @return BelongsTo
     */
    public function maker(): BelongsTo
    {
        return $this->belongsTo(AirplaneMaker::class);
    }
}
