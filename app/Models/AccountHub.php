<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountHub extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_id',
        'airport_id',
        'am_id',
        'purchased_at',
        'turn_over',
        'profits',
        'incidents',
        'incidents_costs'
    ];

    public function airport()
    {
        return $this->belongsTo(Airport::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
