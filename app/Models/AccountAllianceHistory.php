<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountAllianceHistory extends Model
{
    use HasFactory;

    protected $fillable = ['account_id', 'alliance_id', 'enter_at', 'exited_at'];
}
