<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountTask extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_id',
        'am_id',
        'am_label',
        'finish_date'
    ];
}
