<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaintenanceConfiguration extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'account_id',
        'conditions',
        'active',
        'interval_check',
        'last_checked'
    ];

    protected $casts = [
        'conditions' => 'json'
    ];

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }
}
