<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountCircuit extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_id',
        'name',
        'type',
        'status',
        'configuration'
    ];

    protected $casts = [
        'configuration' => 'json'
    ];
}
