<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alliance extends Model
{
    use HasFactory;

    protected $fillable = [
        'am_alliance_id',
        'name',
        'born_at',
        'maximum_discount'
    ];
}
