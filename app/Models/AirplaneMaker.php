<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AirplaneMaker extends Model
{
    use HasFactory;

    protected $fillable = ['am_id', 'name'];

    /**
     * @return HasMany
     */
    public function airplanes(): HasMany
    {
        return $this->hasMany(Airplane::class);
    }
}
