<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountAirplane extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'am_id',
        'airplane_id',
        'account_id',
        'name',
        'configurations',
    ];

    protected $casts = [
        'configurations' => 'json'
    ];
}
