<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'am_account_id',
        'user_id',
        'alliance_id',
        'name',
        'server',
        'born_at',
        'value',
        'balance',
        'airline_configuration',
        'structure_profit',
    ];

    protected $casts = [
        'airline_configuration' => 'json'
    ];


    protected static function booted()
    {
        parent::booted();


        // Cuando se crea una cuenta se verifica si lo que se va a cambiar el es ranking
        static::saved(function (Account $account) {

            AccountAllianceHistory::create([
                'account_id' => $account->id,
                'alliance_id' => $account->alliance_id,
                'entered_at' => Carbon::now()
            ]);

        });


        // Cuando se crea actualiza una cuenta se verifica si lo que se va a cambiar el es ranking
        static::updating(function (Account $account) {


            $currentAccount = Account::whereId($account->id)->first();

            if ($currentAccount->alliance_id !== $account->alliance_id) {

                AccountAllianceHistory::where('account_id', $currentAccount->id)
                    ->where('exited_at', null)
                    ->update([
                        'exited_at' => Carbon::now()
                    ]);
            }

        });

    }

    public function hubs(): HasMany
    {
        return $this->hasMany(AccountHub::class);
    }

    public function maintenance_configurations(): HasMany
    {
        return $this->hasMany(MaintenanceConfiguration::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(AccountTask::class);
    }
}
