<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountRanking extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_id',
        'ranking_number',
        'structural_profit_on_ranking'
    ];

    protected static function booted()
    {
        parent::booted();


        // Cuando se crea una cuenta se verifica si lo que se va a cambiar el es ranking
        static::saved(function (AccountRanking $accountRanking) {

            AccountRankingHistory::create([
                'account_id' => $accountRanking->account_id,
                'ranking_number' => $accountRanking->ranking_number,
                'structural_profit_on_ranking' => $accountRanking->structural_profit_on_ranking
            ]);

        });

    }
}
