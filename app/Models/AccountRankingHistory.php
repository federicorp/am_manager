<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountRankingHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_id',
        'ranking_number',
        'structural_profit_on_ranking'
    ];
}
