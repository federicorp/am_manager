<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Account
 *
 * @property int $id
 * @property string $am_account_id
 * @property int $user_id
 * @property int|null $alliance_id
 * @property string $name
 * @property string $server
 * @property string|null $born_at
 * @property int|null $value
 * @property int|null $balance
 * @property array|null $airline_configuration
 * @property int|null $structure_profit
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AccountHub[] $hubs
 * @property-read int|null $hubs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MaintenanceConfiguration[] $maintenance_configurations
 * @property-read int|null $maintenance_configurations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AccountTask[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Account newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Account newQuery()
 * @method static \Illuminate\Database\Query\Builder|Account onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Account query()
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereAirlineConfiguration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereAllianceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereAmAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereBornAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereStructureProfit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Account whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|Account withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Account withoutTrashed()
 */
	class Account extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountAirplane
 *
 * @property int $id
 * @property string $am_id
 * @property int $airplane_id
 * @property int $account_id
 * @property int|null $account_circuit_id
 * @property string $name
 * @property mixed $configurations
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereAccountCircuitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereAirplaneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereConfigurations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAirplane whereUpdatedAt($value)
 */
	class AccountAirplane extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountAllianceHistory
 *
 * @property int $id
 * @property int $account_id
 * @property int $alliance_id
 * @property string|null $enter_at
 * @property string|null $exited_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory whereAllianceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory whereEnterAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory whereExitedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountAllianceHistory whereUpdatedAt($value)
 */
	class AccountAllianceHistory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountCircuit
 *
 * @property int $id
 * @property int $account_id
 * @property string|null $name
 * @property string $type
 * @property string $status
 * @property mixed|null $configuration
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit whereConfiguration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountCircuit whereUpdatedAt($value)
 */
	class AccountCircuit extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountFinancialStatusHistory
 *
 * @property int $id
 * @property int $account_id
 * @property int|null $turnover
 * @property int|null $flight_costs
 * @property int|null $results
 * @property int|null $loan_payment
 * @property int|null $lease_costs
 * @property int|null $structural_profit
 * @property int|null $rnd_investment
 * @property int|null $staff_cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereFlightCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereLeaseCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereLoanPayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereResults($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereRndInvestment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereStaffCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereStructuralProfit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereTurnover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountFinancialStatusHistory whereUpdatedAt($value)
 */
	class AccountFinancialStatusHistory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountHub
 *
 * @property int $id
 * @property int $account_id
 * @property int $airport_id
 * @property string $am_id
 * @property string $purchased_at
 * @property int $turn_over
 * @property int $profits
 * @property int|null $incidents
 * @property int|null $incidents_costs
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Account $account
 * @property-read \App\Models\Airport $airport
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereAirportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereIncidents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereIncidentsCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereProfits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub wherePurchasedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereTurnOver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHub whereUpdatedAt($value)
 */
	class AccountHub extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountHubRoute
 *
 * @property int $id
 * @property string $am_id
 * @property int $account_hub_id
 * @property int $route_id
 * @property int|null $account_circuit_id
 * @property array|null $last_audit
 * @property array|null $current_prices
 * @property array|null $current_demand
 * @property array|null $remaining_demand
 * @property int $is_frozen
 * @property string|null $locked_until
 * @property string|null $purchase_date
 * @property int|null $purchase_price
 * @property int|null $selling_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\AccountHub $accountHub
 * @property-read \App\Models\Route $route
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereAccountCircuitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereAccountHubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereCurrentDemand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereCurrentPrices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereIsFrozen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereLastAudit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereLockedUntil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute wherePurchaseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute wherePurchasePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereRemainingDemand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereRouteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereSellingPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountHubRoute whereUpdatedAt($value)
 */
	class AccountHubRoute extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountRanking
 *
 * @property int $id
 * @property int $account_id
 * @property int|null $ranking_number
 * @property int|null $structural_profit_on_ranking
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking whereRankingNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking whereStructuralProfitOnRanking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRanking whereUpdatedAt($value)
 */
	class AccountRanking extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountRankingHistory
 *
 * @property int $id
 * @property int $account_id
 * @property int|null $ranking_number
 * @property int|null $structural_profit_on_ranking
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory whereRankingNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory whereStructuralProfitOnRanking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountRankingHistory whereUpdatedAt($value)
 */
	class AccountRankingHistory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountResearch
 *
 * @property int $id
 * @property int $account_id
 * @property int $research_id
 * @property string $started_at
 * @property string|null $ended_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch whereResearchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountResearch whereUpdatedAt($value)
 */
	class AccountResearch extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountService
 *
 * @property int $id
 * @property int $service_id
 * @property int $researched
 * @property int $active
 * @property string $activated_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService whereActivatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService whereResearched($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountService whereUpdatedAt($value)
 */
	class AccountService extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccountTask
 *
 * @property int $id
 * @property int $account_id
 * @property string $am_id
 * @property string $am_label
 * @property string $finish_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask whereAmLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask whereFinishDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountTask whereUpdatedAt($value)
 */
	class AccountTask extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Airplane
 *
 * @property int $id
 * @property string $am_id
 * @property string $name
 * @property int $airplane_maker_id
 * @property array $configurations
 * @property float $tonnage
 * @property int $range
 * @property float $speed
 * @property array $available_in
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\AirplaneMaker|null $maker
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane query()
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereAirplaneMakerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereAvailableIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereConfigurations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereSpeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereTonnage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airplane whereUpdatedAt($value)
 */
	class Airplane extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AirplaneMaker
 *
 * @property int $id
 * @property string $am_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Airplane[] $airplanes
 * @property-read int|null $airplanes_count
 * @method static \Illuminate\Database\Eloquent\Builder|AirplaneMaker newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AirplaneMaker newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AirplaneMaker query()
 * @method static \Illuminate\Database\Eloquent\Builder|AirplaneMaker whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirplaneMaker whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirplaneMaker whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirplaneMaker whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AirplaneMaker whereUpdatedAt($value)
 */
	class AirplaneMaker extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Airport
 *
 * @property int $id
 * @property string $am_id
 * @property string $name
 * @property string $iata_code
 * @property int $category
 * @property int|null $price
 * @property float|null $latitude
 * @property float|null $longitude
 * @property int $country_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Airport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Airport newQuery()
 * @method static \Illuminate\Database\Query\Builder|Airport onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Airport query()
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereIataCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Airport whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Airport withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Airport withoutTrashed()
 */
	class Airport extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Alliance
 *
 * @property int $id
 * @property string $am_alliance_id
 * @property string $name
 * @property string|null $born_at
 * @property float|null $maximum_discount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance query()
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance whereAmAllianceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance whereBornAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance whereMaximumDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Alliance whereUpdatedAt($value)
 */
	class Alliance extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AllianceRanking
 *
 * @property int $id
 * @property int $alliance_id
 * @property int $ranking
 * @property mixed $ranking_info
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking query()
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking whereAllianceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking whereRanking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking whereRankingInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRanking whereUpdatedAt($value)
 */
	class AllianceRanking extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AllianceRankingHistory
 *
 * @property int $id
 * @property int $alliance_id
 * @property int $ranking
 * @property mixed|null $ranking_info
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory whereAllianceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory whereRanking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory whereRankingInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AllianceRankingHistory whereUpdatedAt($value)
 */
	class AllianceRankingHistory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Continent
 *
 * @property int $id
 * @property string $am_id
 * @property string $name
 * @property string $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Continent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Continent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Continent query()
 * @method static \Illuminate\Database\Eloquent\Builder|Continent whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Continent whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Continent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Continent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Continent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Continent whereUpdatedAt($value)
 */
	class Continent extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Country
 *
 * @property int $id
 * @property int $continent_id
 * @property string $am_id
 * @property string|null $old_am_id
 * @property string $code
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereContinentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereOldAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereUpdatedAt($value)
 */
	class Country extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\GroupPurchase
 *
 * @property int $id
 * @property int $alliance_id
 * @property mixed|null $gp_info
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|GroupPurchase newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GroupPurchase newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GroupPurchase query()
 * @method static \Illuminate\Database\Eloquent\Builder|GroupPurchase whereAllianceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupPurchase whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupPurchase whereGpInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupPurchase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupPurchase whereUpdatedAt($value)
 */
	class GroupPurchase extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Maintenance
 *
 * @property int $id
 * @property int $account_id
 * @property string $performed_at
 * @property int $cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance query()
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance wherePerformedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Maintenance whereUpdatedAt($value)
 */
	class Maintenance extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MaintenanceConfiguration
 *
 * @property int $id
 * @property int $account_id
 * @property array $conditions
 * @property int $active
 * @property int $interval_check
 * @property string|null $last_checked
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Account $account
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration newQuery()
 * @method static \Illuminate\Database\Query\Builder|MaintenanceConfiguration onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration query()
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereConditions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereIntervalCheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereLastChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MaintenanceConfiguration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|MaintenanceConfiguration withTrashed()
 * @method static \Illuminate\Database\Query\Builder|MaintenanceConfiguration withoutTrashed()
 */
	class MaintenanceConfiguration extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Research
 *
 * @property int $id
 * @property string $am_id
 * @property string $name
 * @property int $research_type_id
 * @property int $cost
 * @property string $time_develop
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Research newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Research newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Research query()
 * @method static \Illuminate\Database\Eloquent\Builder|Research whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Research whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Research whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Research whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Research whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Research whereResearchTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Research whereTimeDevelop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Research whereUpdatedAt($value)
 */
	class Research extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ResearchType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ResearchType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResearchType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResearchType query()
 * @method static \Illuminate\Database\Eloquent\Builder|ResearchType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResearchType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResearchType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResearchType whereUpdatedAt($value)
 */
	class ResearchType extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Route
 *
 * @property int $id
 * @property int $airport_from_id
 * @property int $airport_to_id
 * @property float $distance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Route newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Route newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Route query()
 * @method static \Illuminate\Database\Eloquent\Builder|Route whereAirportFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Route whereAirportToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Route whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Route whereDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Route whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Route whereUpdatedAt($value)
 */
	class Route extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Service
 *
 * @property int $id
 * @property string $am_id
 * @property string $name
 * @property mixed|null $more_info
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereAmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereMoreInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereUpdatedAt($value)
 */
	class Service extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent implements \OwenIt\Auditing\Contracts\Auditable {}
}

