<?php

use App\Http\Controllers\AccountsController;
use App\Http\Controllers\Auth\ConfirmAccountController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\PasswordRecoveryController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CircuitsController;
use App\Http\Controllers\MaintenancesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** -- Not Logged-in User Routes -- **/

// Login Route
Route::post('login', [LoginController::class, 'login']);
// Register Route
Route::post('register', [RegisterController::class, 'register']);

// Password Recovery Route
Route::post('reset-password', [PasswordRecoveryController::class, 'sendRecover']);
Route::get('reset-password/{token:token}', [PasswordRecoveryController::class, 'verify'])
    ->missing(fn() => response()->error(404, 'Token no es válido o expiró.'));
Route::post('reset-password/{token:token}/recover', [PasswordRecoveryController::class, 'recover'])
    ->missing(fn() => response()->error(404, 'Token no es válido o expiró.'));


// Account confirmation Route
Route::get('confirm-account/{token:token}', [ConfirmAccountController::class, 'confirm'])
    ->missing(fn() => response()->error(404, 'Token no es válido o expiró.'));

/** -- Logged-in User Routes -- **/

Route::middleware(['auth:sanctum'])->group(function () {

    Route::apiResource('accounts', AccountsController::class);

    Route::prefix('accounts/{account}')->group(function() {

        Route::get('/maintenance-configurations', [MaintenancesController::class, 'indexConfigurations']);
        Route::post('/maintenance-configurations', [MaintenancesController::class, 'createConfiguration']);
        Route::put('/maintenance-configurations/{maintenanceConfiguration}', [MaintenancesController::class, 'updateConfiguration']);

        // Circuitos
        Route::apiResource('/circuits', CircuitsController::class);

    });

    // Logout Route
    Route::post('logout', [LogoutController::class, 'logout']);
});

Route::fallback(fn() => response()->error(404, 'Not Found'));
