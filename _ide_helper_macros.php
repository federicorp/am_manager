<?php

namespace {
    exit('This file should not be included, only analyzed by your IDE');
}

namespace Illuminate\Http {

    /**
     * @method JsonResponse error(int $statusCode, string|null $message = '', $data = []) Retorna error.
     * @method JsonResponse success(int $statusCode = 200, string|null $message = '', $data = [], $paginate = false) Retorna success.
     */
    class Response {}
}
