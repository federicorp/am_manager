/etc/crontab

* * * * * www-data cd /var/www/am-be && php artisan schedule:run >> /dev/null 2>&1
15 0 * * *      root    cd /var/www/am-be && chown root.www-data -R storage/logs/ && chmod 664 storage/logs/*.log
